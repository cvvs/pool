package pool_test

// TODO: Fix the tests.

import (
	"context"
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/cvvs/pool"
)

func TestDefaultPool(t *testing.T) {
	count := 10
	td := testData(count)
	var triggered int32 = 0

	p, err := pool.New(2, createTestWorker(nameChan(), 100*time.Millisecond, &triggered))
	if err != nil {
		t.Fatal("failed to create pool:", err)
	}
	var wg sync.WaitGroup

	ctx := context.TODO()

	for i := 0; i < count; i++ {

		worker, err := p.Get(ctx)
		if err != nil {
			t.Errorf("error getting worker %d: %s", i, err)
		}

		wg.Add(1)
		go func(w pool.Worker, x int) {
			defer wg.Done()
			w.Do(ctx)
			t.Logf("test %d active workers: %d", x, p.Active())

			to, tocancel := context.WithTimeout(ctx, 20*time.Millisecond)
			defer tocancel()
			if err := p.Return(to, w); err != nil {
				t.Errorf("timeout returning worker %d: %s", x, err)
			} else {
				t.Log("worker", x, "returned")
			}
		}(worker, i)

		_, ok := worker.(*testWorker)
		if !ok {
			t.Errorf("Expected type %T, but got type %T", testWorker{}, worker)
			continue
		}
		a := p.Active()
		s := p.Size()
		// TODO:  This is racy. Active can exceed size by a small number for short periods of time
		if a > s {
			t.Logf("active workers %d exceeds size %d", a, s)
		}
	}
	close(td)
	wg.Wait()

	if v := int(atomic.LoadInt32(&triggered)); v != count {
		t.Errorf("expect %d workers launched, but only %d counted", count, v)
	}

}

func TestUpsizePool(t *testing.T) {
	count := 100
	poolsize := 2
	td := testData(count)
	var triggered int32 = 0

	p, err := pool.New(poolsize, createTestWorker(nameChan(), 100*time.Millisecond, &triggered))
	if err != nil {
		t.Fatal("failed to create pool:", err)
	}
	var wg sync.WaitGroup

	ctx := context.TODO()

	for i := 0; i < count; i++ {

		if i == 20 {
			t.Log("resizing pool to", 3*poolsize)
			if err = p.SetSize(3 * poolsize); err != nil {
				t.Fatal("error resizing pool:", err)
			}
		}

		worker, err := p.Get(ctx)
		if err != nil {
			t.Errorf("error getting worker %d: %s", i, err)
		}

		wg.Add(1)
		go func(w pool.Worker, x int) {
			defer wg.Done()
			w.Do(ctx)
			t.Logf("test %d active workers: %d", x, p.Active())

			to, tocancel := context.WithTimeout(ctx, 20*time.Millisecond)
			defer tocancel()
			if err := p.Return(to, w); err != nil {
				t.Errorf("timeout returning worker %d: %s", x, err)
			} else {
				t.Log("worker", x, "returned")
			}
		}(worker, i)

		_, ok := worker.(*testWorker)
		if !ok {
			t.Errorf("Expected type %T, but got type %T", testWorker{}, worker)
			continue
		}
		a := p.Active()
		s := p.Size()
		// TODO:  This is racy. Active can exceed size by a small number for short periods of time
		if a > s {
			t.Logf("active workers %d exceeds size %d", a, s)
		}

		if i >= 20 && p.Size() != 3*poolsize {
			t.Errorf("pool size %d != %d", p.Size(), 3*poolsize)
		}
	}
	close(td)
	wg.Wait()

	if v := int(atomic.LoadInt32(&triggered)); v != count {
		t.Errorf("expect %d workers launched, but only %d counted", count, v)
	}

}

func TestDownsizePool(t *testing.T) {
	count := 20
	poolsize := 4
	td := testData(count)
	var triggered int32 = 0

	p, err := pool.New(poolsize, createTestWorker(nameChan(), 5*time.Millisecond, &triggered))
	if err != nil {
		t.Fatal("failed to create pool:", err)
	}
	var wg sync.WaitGroup

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	for i := 0; i < count; i++ {

		if i == count/2 {
			t.Log("resizing pool to", poolsize/2)
			if err = p.SetSize(poolsize / 2); err != nil {
				t.Fatal("error resizing pool:", err)
			}
		}

		worker, err := p.Get(ctx)
		if err != nil {
			t.Errorf("error getting worker %d: %s", i, err)
		}

		wg.Add(1)
		go func(w pool.Worker, x int) {
			defer wg.Done()
			w.Do(ctx)
			//t.Logf("test %d active workers: %d", x, p.Active())

			to, tocancel := context.WithTimeout(ctx, 20*time.Millisecond)
			defer tocancel()

			if err := p.Return(to, w); err != nil {
				t.Errorf("timeout returning worker %d: %s", x, err)
			} else {
				t.Log("worker", x, "returned")
			}
		}(worker, i)

		_, ok := worker.(*testWorker)
		if !ok {
			t.Errorf("Expected type %T, but got type %T", testWorker{}, worker)
			continue
		}
		/*
			a := p.Active()
			s := p.Size()
			// TODO:  This is racy. Active can exceed size by a small number for short periods of time
			if a > s {
				t.Logf("active workers %d exceeds size %d", a, s)
			}

			if i >= count/2 && p.Size() != poolsize/2 {
				t.Errorf("pool size %d != %d", p.Size(), poolsize/2)
			} */
	}
	close(td)
	wg.Wait()

	if v := int(atomic.LoadInt32(&triggered)); v != count {
		t.Errorf("expect %d workers launched, but only %d counted", count, v)
	}

}

// Support Stuff

//generate some testdata
func testData(size int) chan int {
	td := make(chan int, size)
	for i := 0; i < size; i++ {
		td <- i
	}
	return td
}

func nameChan() chan string {
	nc := make(chan string)
	go func() {
		for i := 0; ; i++ {
			nc <- fmt.Sprintf("%d", i)
		}
	}()
	return nc
}

//create a test worker that takes ints from input and puts them to output
func createTestWorker(names chan string, sleepyTime time.Duration, counter *int32) pool.NewWorkerFn {
	fn := func() (pool.Worker, error) {
		n := <-names
		var w = testWorker{name: n, sleep: sleepyTime, counter: counter}
		return &w, nil
	}
	return fn
}

type testWorker struct {
	name    string
	sleep   time.Duration
	counter *int32
}

func (w *testWorker) Do(context.Context) error {
	atomic.AddInt32(w.counter, 1)
	time.Sleep(w.sleep)
	return nil
}
