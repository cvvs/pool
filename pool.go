package pool

import (
	"context"
	"errors"
	"sync"
	"sync/atomic"
)

const (
	bufferNotRunning int32 = 0
	bufferRunning    int32 = 1
)

//Worker does a thing
type Worker interface {
	Do(context.Context) error
}

//NewWorkerFn creates a Worker or returns an error
type NewWorkerFn func() (Worker, error)

//WorkerBuffer uses a buffered channel of size Size to hold workers, and thus it can be as large as memory allows.
type WorkerBuffer struct {
	Size    int
	Active  int32
	In      <-chan Worker // Never change these
	Out     chan<- Worker // Never change these
	Buffer  chan Worker
	Exit    chan int8
	Running int32
}

//NewWorkerBuffer creats a WorkerBuffer of size size with the provided in and out channels
func NewWorkerBuffer(size int, in <-chan Worker, out chan<- Worker) *WorkerBuffer {
	wb := WorkerBuffer{
		Size:    size,
		Buffer:  make(chan Worker, size),
		Exit:    make(chan int8),
		Running: bufferNotRunning,
		In:      in,
		Out:     out,
		Active:  0,
	}
	return &wb
}

//Run connects the in and out channels with a buffered channel, pulling from in and pushing to out
//
// TODO: Two goroutines were added because when there was only a single select with all of the channels,
// the "last" workers created always seemed to deadlock on return. Seperating in and out into seperate
// routines fixed that.
//
// TODO: Replace anonymous functions with real.
// TODO: Should this take a context, or just a little too paranoid about deadlocks on shutdown?
func (buf *WorkerBuffer) Run() {
	atomic.StoreInt32(&buf.Running, bufferRunning)

	wg := new(sync.WaitGroup)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case w := <-buf.In:
				buf.Buffer <- w
				atomic.AddInt32(&buf.Active, -1)
			case <-buf.Exit:
				return
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			// This will deadlock waiting for a client without another select.'
			// there is likely a much better solution than this.
			case w := <-buf.Buffer:
				select {
				case buf.Out <- w:
					atomic.AddInt32(&buf.Active, 1)
				case <-buf.Exit:
					buf.Buffer <- w
					return
				}
			case <-buf.Exit:
				return
			}
		}
	}()
	wg.Wait()
	close(buf.Buffer)
	atomic.StoreInt32(&buf.Running, bufferNotRunning)
}

//Stop the buffer so it can be drained
func (buf *WorkerBuffer) Stop() error {
	if buf.IsRunning() {
		// TODO: this can be better
		for i := 0; i < 2; i++ {
			buf.Exit <- 0
		}
	} else {
		return errors.New("buffer is already stopped")
	}
	return nil
}

//IsRunning returns true if the buffer is running
func (buf *WorkerBuffer) IsRunning() bool {
	return atomic.LoadInt32(&buf.Running) == bufferRunning
}

//Resize changes the size of the buffer. Resize will drain excess workers or create new workers as required.
//
// TODO: Should the existing buffer continue running on an error, instead of stopping?
func (buf *WorkerBuffer) Resize(ctx context.Context, fn NewWorkerFn, size int) (*WorkerBuffer, error) {
	//If it's the same size, just return the current buffer.
	if size == buf.Size {
		return buf, nil
	}
	buf.Stop() // Stop the buffer to prevent concurrency problems.

	newbuf := new(WorkerBuffer)
	newbuf.Active = buf.Active
	newbuf.Exit = buf.Exit
	newbuf.In = buf.In
	newbuf.Out = buf.Out
	newbuf.Size = size
	newbuf.Buffer = make(chan Worker, size)

	drained := make([]Worker, 0, max(buf.Size, newbuf.Size))

	for w := range buf.Buffer {
		drained = append(drained, w)
	}

	//This assumes a full buffer of Size workers. If desired, that could be
	//checked with more complex logic (see previous commits)
	switch {

	case buf.Size > newbuf.Size: // Drain some stuff
		// If there are more active than size, wait for the excess to complete
		// Do this in the background. What can go wrong?
		// TODO: If a resize while draining occurs, this _could_ be problematic
		// so locking may need to be added. In short, if a downsize happens
		// while a drain is occuring, too many drains could occur.
		// It may make sense to drain elsewhere. It may also make make sense to lock
		// when an item is pulled from or put to the buffer.
		go func() {
			toDrain := int(newbuf.Active) - newbuf.Size
			for i := 0; i < toDrain; i++ {
				<-buf.In
				newbuf.Active--
			}
		}()

		// Take any others we need from the buffer.
		// TODO: check for index OOB
		for i := int(newbuf.Active); i < newbuf.Size; i++ {
			newbuf.Buffer <- drained[i]
		}

	case buf.Size <= newbuf.Size: // Dump current workers into the buffer, and create any required

		for i := range drained {
			newbuf.Buffer <- drained[i]
		}

		for i := newbuf.Size - buf.Size; i > 0; i-- {
			w, err := fn()
			if err != nil {
				return nil, err
			}
			newbuf.Buffer <- w
		}
	}

	go newbuf.Run()
	return newbuf, nil
}

//ActiveWorkers returns the workers which have been allocated
func (buf *WorkerBuffer) ActiveWorkers() int32 {
	return atomic.LoadInt32(&buf.Active)
}

//Pool contains a pool of workers. The worker should do a single thing, and then return. While the worker itself
//can be long lived, the Do function should just peform an expected task and return.
//
// Note: "Dead" workers are not replaced, so design worker.Do() such that it cannot "kill" a worker.
//
// Warning: Always call pool.New()
//
//TODO: Pool should likely be initialized with a Run() or Init() function to initialize the private fields. Alternatively,
//Pool can be made private, but that seems rather annoying to use.
type Pool struct {
	*sync.RWMutex
	NewWorker NewWorkerFn
	out       chan Worker
	buffer    *WorkerBuffer
	in        chan Worker
}

//Get returns the next available worker.
func (p *Pool) Get(ctx context.Context) (Worker, error) {
	for {
		select {
		case w := <-p.out:
			return w, nil
		case <-ctx.Done():
			return nil, ctx.Err()
		}
	}
}

// Return a worker back to the pool
// Workers _must_ be returned when done, or you will run out of
// workers.
// TODO: Does Return() need to be renamed?
func (p *Pool) Return(ctx context.Context, w Worker) error {
	select {
	case p.in <- w:
	case <-ctx.Done():
		return ctx.Err()
	}
	return nil
}

//Size returns the size of the pool
func (p *Pool) Size() int {
	p.RLock()
	defer p.RUnlock()
	return p.buffer.Size
}

//SetSize sets the size of worker pool.
func (p *Pool) SetSize(size int) error {
	p.Lock()
	defer p.Unlock()

	if size < 1 {
		return errors.New("size must be at least one")
	} else if size == p.buffer.Size {
		return nil
	}
	newBuffer, err := p.buffer.Resize(context.TODO(), p.NewWorker, size)
	if err != nil {
		return err
	}
	p.buffer = newBuffer
	return nil
}

//Active count of workers
func (p *Pool) Active() int {
	p.RLock()
	defer p.RUnlock()
	return int(p.buffer.ActiveWorkers())
}

//New Pool of size. Workers are created with NewWorkerFn fn
func New(size int, fn NewWorkerFn) (*Pool, error) {

	var p Pool
	p.RWMutex = new(sync.RWMutex)
	p.NewWorker = fn
	p.in = make(chan Worker)
	p.out = make(chan Worker)
	p.buffer = NewWorkerBuffer(size, p.in, p.out)

	// TODO: I don't like this.
	for i := 0; i < size; i++ {
		w, err := p.NewWorker()
		if err != nil {
			return nil, err
		}
		p.buffer.Buffer <- w
	}

	go p.buffer.Run()
	return &p, nil
}

//helper to return the larger of the two ints
func max(a, b int) int {
	if b > a {
		return b
	}
	return a
}
