# pool

pool provides a dynamically sizeable worker pool using a worker buffer.

The pool takes a function used create workers when the size is expanded
and when the pool is created. That's probably a bad idea, and adding 
workers should be done by the caller. 

## TODO
- Refactor pool resize tests to use a done context so that the tests
  are actually valid
- Docs
- WorkerBuffer tests

## notes
- The worker buffer could be replaced with internal pool code, but it encapsulates some things